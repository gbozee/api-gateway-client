#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys


try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

version = 0.1

setup(
    name='tuteria-api-client',
    version=str(version),
    author='',
    author_email='gbozee@gmail.com',
    packages=[
        'gateway_client',
    ],
    include_package_data=True,
    install_requires=[
        'requests',
    ],
    zip_safe=False,
)
import os
from ..base import TuteriaApiException
from ..client3 import GraphQLClient


class Subjects(object):
    def __init__(self, url):
        self.base_url = os.environ.get('GRAPHQL_ENDPOINT')
        if url:
            self.base_url = url
        if not self.base_url:
            raise TuteriaApiException("Url for Graphql Service was not found.")
        self.client = GraphQLClient(self.base_url)

    def get_parent_skills(self):
        result = self.client.execute("""
            query parentSkills{
                skills(parent_subjects:true){
                name
                }
            }
        """, {}, "parentSkills")
        result = self._get_response(result, "skills")
        return [r['name'] for r in result]

    def _get_response(self, result, key):
        response = result['data'][key]
        return response

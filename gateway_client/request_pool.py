from . import BaseGraphClient
import json


class RequestPool(object):
    fields = """
        id
        tutor{
            first_name
            last_name
        }
        subjects
        cost
        recommended
        request_status
        default_subject
        approved
    """

    def __init__(self, request_id, client):
        self.request_id = request_id
        self.client = client

    def get_tutor(self, query, tutor_slug):
        result = self._execute(
            query,
            {"request_id": self.request_id, 'tutor_slug': tutor_slug},
            "SingleRequestPool")
        data = self._get_response(result, 'pool_instance')
        return {
            'pk': data['id'],
            'tutor_slug': data['tutor']['username'],
        }

    def create_dummy_pool(self, query, **kwargs):
        kwargs.update(req_id=self.request_id)
        result = self._execute(query, kwargs, "createDummyPool")
        data = self._get_response(result, "create_dummy_pool", "pool_instance")
        return {
            'pk': data['id'],
            'tutor_slug': data['tutor']['username']
        }

    def create_and_add_tutor_to_pool(self, query, **kwargs):
        kwargs.update(req_id=self.request_id)
        result = self._execute(query, kwargs, "addTutorToPool")

        data = self._get_response(result, "add_tutor_to_pool", "pool_instance")
        return data

    def send_profile_to_client(self, **kwargs):
        pass

    def _execute(self, *args):
        if self.client.change:
            query, first_args = self.client._transform_query(*args)
            return self.client._execute(query, first_args)
        
        return self.client._execute(*args)

    def _get_response(self, result, *args):
        # import pdb; pdb.set_trace()
        return self.client._get_response(result, *args)

    def approved_tutors(self, query, **kwargs):
        temp = self._execute(
            query, {"request_id": self.request_id}, "ApprovedTutors")
        result = self._get_response(temp, "approved_tutors")
        # if result:
        #     return [ToDict(x) for x in result]
        # return []
        return result

    def approved_tutors_teach_all_subjects(self, query, **kwargs):
        temp = self._execute(
            query, {"request_id": self.request_id}, "ApprovedTutors")
        result = self._get_response(temp, "approved_tutors_teach_all_subjects")
        # if result:
        #     return [ToDict(x) for x in result]
        # return []
        return result


class MatchingService(BaseGraphClient):
    fields = """
        id
        tutor{
            first_name
            last_name
            location
        }
        subjects
        cost
        recommended
        request_status
        default_subject
        approved
    """

    def init_instance(self, request_id):
        """initializes the RequstPool class which is
        used to make queries for other methods

        Args:
            request_id (Int): The request_id from the
            BaseRequestTutor model
        """
        self.instance = RequestPool(request_id, self)
        return self.instance

    def create_dummy_pool(self,  **kwargs):
        params = "(tutor_slug:$tutor_slug,approved:$approved,req_id:$req_id,create:true)"
        actual_params = """
        {
            create_dummy_pool%s {
                pool_instance{
                    id
                    tutor{
                        username
                    }
                }

            }
        }""" % params
        self.query = "mutation createDummyPool"
        if not self.change:
            self.query = self.query + \
                "($tutor_slug:String,$approved:Boolean,$req_id:Int)"
        self.query = self.query + actual_params

        return self.instance.create_dummy_pool(self.query, **kwargs)

    def get_tutor_for_request(self, tutor_slug):
        """Fetches the tutor that applied to the request pool

        Args:
            tutor_slug (str): The slug of the tutor that applied

        Returns:
            dict: A dictionary of the request pool instance which
            contains the details of the tutor.
        """
        self.query = """{
            pool_instance(request_id:$request_id,tutor_slug:$tutor_slug){
                id
                tutor {
                    first_name
                    last_name
                    username

                }
            }
        }
        """
        if not self.change:
            self.query = """
            query SingleRequestPool($request_id: Int!,$tutor_slug:String)%s
            """ % self.query
        return self.instance.get_tutor(self.query, tutor_slug)

    def approved_tutors(self, **kwargs):

        self.query = """
        {
            approved_tutors(request_id:$request_id){
                %s
            }
        }
        """ % self.fields
        if not self.change:
            self.query = """
            query ApprovedTutors($request_id: Int)%s
            """ % self.query
        return self.instance.approved_tutors(self.query, **kwargs)

    def approved_tutors_teach_all_subjects(self, **kwargs):
        self.query = """
        {
            approved_tutors_teach_all_subjects(request_id:$request_id){
                %s
            }
        }
        """ % self.fields
        if not self.change:
            self.query = """
            query ApprovedTutors($request_id: Int!)%s
            """ % self.query

        return self.instance.approved_tutors_teach_all_subjects(
            self.query, **kwargs)

    def create_and_add_tutor_to_pool(self, **kwargs):
        params = "(request_subjects:$request_subjects,tutor_slug:$tutor_slug,cost:$cost,req_id:$req_id,notify_job:true)"
        actual_query = """{
            add_tutor_to_pool%s{
                pool_instance{
                    id
                    tutor{
                        username
                    }
                    cost
                }
            }
        }""" % params
        self.query = "mutation addTutorToPool"
        if not self.change:
            self.query = self.query + \
                "($request_subjects:[String],$tutor_slug:String,$cost:Float,$req_id:Int)"
        self.query = self.query + actual_query
        return self.instance.create_and_add_tutor_to_pool(self.query, **kwargs)

    def send_profile_to_client(self, **kwargs):
        return self.instance.send_profile_to_client(**kwargs)
